import 'package:my_notes/data/models/note_ans_tags_model.dart';
import 'package:my_notes/domain/repositories/repository_impl.dart';

class GetNotesUseCase {
  final Repository repository;

  GetNotesUseCase(this.repository);

  Future<List<NoteAndTagsModel>> getNotes() async =>
      await repository.getNotes();
}

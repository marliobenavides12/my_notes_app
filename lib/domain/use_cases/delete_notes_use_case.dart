import 'package:my_notes/domain/repositories/repository_impl.dart';

class DeleteNotesUseCase {
  final Repository repository;

  DeleteNotesUseCase(this.repository);

  Future<bool> deleteNotes() async => await repository.deleteNotes();
}

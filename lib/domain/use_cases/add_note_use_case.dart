import 'package:my_notes/data/models/note_ans_tags_model.dart';
import 'package:my_notes/domain/repositories/repository_impl.dart';

class AddNoteUseCase {
  final Repository repository;

  AddNoteUseCase(this.repository);

  Future<bool> addNote(NoteAndTagsModel newNote) async =>
      await repository.addNote(newNote);
}

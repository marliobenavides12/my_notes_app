import 'package:my_notes/data/models/note_ans_tags_model.dart';
import 'package:my_notes/domain/repositories/repository_impl.dart';

class UpdateNoteUseCase {
  final Repository repository;

  UpdateNoteUseCase(this.repository);

  Future<bool> deleteNote(NoteAndTagsModel newNote) async =>
      await repository.updateNote(newNote);
}

import 'package:my_notes/data/models/note_ans_tags_model.dart';

abstract class Repository {
  Future<List<NoteAndTagsModel>> getNotes();
  Future<bool> addNote(NoteAndTagsModel newNote);
  Future<bool> updateNote(NoteAndTagsModel note);
  Future<bool> deleteNote(NoteAndTagsModel note);
  Future<bool> deleteNotes();
}

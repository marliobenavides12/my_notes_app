import 'package:flutter/material.dart';

import 'package:my_notes/injection_container.dart';
import 'package:my_notes/presentation/bloc/bloc_exports.dart';
import 'package:my_notes/presentation/pages/splash/splash_page.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await initDependencies();

  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider<HomeBloc>(create: (context) => sl<HomeBloc>()),
        BlocProvider<SplashBloc>(create: (context) => sl<SplashBloc>()),
        BlocProvider<NewNoteBloc>(create: (context) => sl<NewNoteBloc>()),
      ],
      child: MaterialApp(
        home: const SplashPage(),
        title: 'Flutter Demo',
        debugShowCheckedModeBanner: false,
        theme: ThemeData(
          useMaterial3: true,
          colorScheme: ColorScheme.fromSeed(seedColor: Colors.deepPurple),
        ),
      ),
    );
  }
}

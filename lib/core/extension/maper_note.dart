import 'package:my_notes/data/models/tag_model.dart';
import 'package:my_notes/data/models/note_model.dart';
import 'package:my_notes/data/models/note_ans_tags_model.dart';

extension MaperNoteModel on NoteModel {
  NoteAndTagsModel joinModels(List<TagModel> tags) => NoteAndTagsModel(
        tags: tags,
        note: NoteModel(
          id: id,
          body: body,
          title: title,
          dateCreated: dateCreated,
        ),
      );
}

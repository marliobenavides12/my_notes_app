import 'package:my_notes/data/models/tag_model.dart';
import 'package:my_notes/data/models/note_model.dart';
import 'package:my_notes/data/models/note_ans_tags_model.dart';

extension MaperNoteAndTagsModel on NoteAndTagsModel {
  NoteAndTagsModel joinModels({
    required NoteModel note,
    List<TagModel> tags = const [],
  }) =>
      NoteAndTagsModel(tags: tags, note: note);
}

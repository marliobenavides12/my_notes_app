import 'package:flutter/material.dart';

class AppColors {
  AppColors._();

  static const Color title = Color(0xFF333333);
  static const Color subTitle = Color(0xFF666666);

  // static Color error = Color(0xFFDC3545);
  static const Color error = Color(0xFFE1665D);
  static const Color success = Color(0xFF28A746);
  static const Color warning = Color(0xFFfd7e14);

  static const Color darkGrey = Color(0xFF767889);
  static const Color lightGrey = Color(0xFFEAECEF);
  static const Color mediumGrey = Color(0xFFB8BACB);

  static const Color darkBlue = Color(0xFF175ECA);
  static const Color lightBlue = Color(0xFF546FEA);
  static const Color mediumBlue = Color(0xFF4767EE);

  static const Color primaryBlue = Color(0xFF4767EE);
  static const Color backgroundColor = Color(0xFFF3F3F3);
}

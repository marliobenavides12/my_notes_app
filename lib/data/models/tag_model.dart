import 'package:floor/floor.dart';
import 'package:equatable/equatable.dart';

@Entity(tableName: 'tags', primaryKeys: ['id'])
class TagModel extends Equatable {
  final String id;
  final String title;
  final String noteId;

  const TagModel({
    required this.id,
    required this.title,
    required this.noteId,
  });

  factory TagModel.formNoData() => const TagModel(
        id: '',
        title: '',
        noteId: '',
      );

  @override
  List<Object?> get props => [id, title, noteId];
}

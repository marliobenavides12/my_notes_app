import 'package:equatable/equatable.dart';

import 'package:my_notes/data/models/tag_model.dart';
import 'package:my_notes/data/models/note_model.dart';

class NoteAndTagsModel extends Equatable {
  final NoteModel note;
  final List<TagModel> tags;

  const NoteAndTagsModel({required this.note, required this.tags});

  factory NoteAndTagsModel.formNoData() => NoteAndTagsModel(
        tags: const [],
        note: NoteModel.formNoData(),
      );

  @override
  List<Object?> get props => [note, tags];
}

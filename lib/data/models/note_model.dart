import 'package:floor/floor.dart';
import 'package:equatable/equatable.dart';

@Entity(tableName: 'notes', primaryKeys: ['id'])
class NoteModel extends Equatable {
  final String id;
  final String body;
  final String title;
  final String dateCreated;

  const NoteModel({
    required this.id,
    required this.body,
    required this.title,
    required this.dateCreated,
  });

  factory NoteModel.formNoData() => const NoteModel(
        id: '',
        body: '',
        title: '',
        dateCreated: '',
      );

  @override
  List<Object?> get props => [id, body, title, dateCreated];
}

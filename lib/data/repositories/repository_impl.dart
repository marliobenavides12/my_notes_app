import 'package:my_notes/data/datasources/datasource.dart';
import 'package:my_notes/data/models/note_ans_tags_model.dart';
import 'package:my_notes/domain/repositories/repository_impl.dart';

class RepositoryImpl extends Repository {
  final DataSource datasource;

  RepositoryImpl(this.datasource);

  @override
  Future<List<NoteAndTagsModel>> getNotes() async {
    final List<NoteAndTagsModel> value = await datasource.getNotes();
    return value;
  }

  @override
  Future<bool> addNote(NoteAndTagsModel newNote) async {
    final bool value = await datasource.addNote(newNote);
    return value;
  }

  @override
  Future<bool> deleteNote(NoteAndTagsModel note) async {
    final bool value = await datasource.deleteNote(note);
    return value;
  }

  @override
  Future<bool> deleteNotes() async {
    final bool value = await datasource.deleteNotes();
    return value;
  }

  @override
  Future<bool> updateNote(NoteAndTagsModel note) async {
    final bool value = await datasource.deleteNote(note);
    return value;
  }
}

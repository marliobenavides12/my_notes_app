import 'package:my_notes/core/extension/maper_note.dart';
import 'package:my_notes/data/models/note_ans_tags_model.dart';
import 'package:my_notes/data/datasources/local/app_database.dart';
import 'package:my_notes/data/models/tag_model.dart';

abstract class DataSource {
  Future<List<NoteAndTagsModel>> getNotes();
  Future<bool> addNote(NoteAndTagsModel newNote);
  Future<bool> updateNote(NoteAndTagsModel note);
  Future<bool> deleteNote(NoteAndTagsModel note);
  Future<bool> deleteNotes();
}

class DataSourceImpl extends DataSource {
  final AppDatabase appDatabase;

  DataSourceImpl(this.appDatabase);

  @override
  Future<List<NoteAndTagsModel>> getNotes() async {
    final tags = await appDatabase.tagDao.getTags();
    final notes = await appDatabase.noteDao.getNotes();

    List<NoteAndTagsModel> response = [];

    notes.forEach((note) {
      if (tags.isNotEmpty) {
        List<TagModel> addTags = [];
        tags.forEach((tag) {
          if (note.id == tag.noteId) {
            addTags.add(tag);
          }
        });
        response.add(note.joinModels(addTags));
      } else {
        response.add(NoteAndTagsModel(note: note, tags: const []));
      }
    });

    return response;
  }

  @override
  Future<bool> addNote(NoteAndTagsModel newNote) async {
    await appDatabase.noteDao.insertNote(newNote.note);
    await appDatabase.tagDao.insertTags(newNote.tags);

    return true;
  }

  @override
  Future<bool> deleteNote(NoteAndTagsModel note) async {
    await appDatabase.tagDao.deleteTags(note.tags);
    await appDatabase.noteDao.deleteNote(note.note);

    final datas = await appDatabase.noteDao.getNotes();
    final valid = datas.any((element) => element.id == note.note.id);

    return valid;
  }

  @override
  Future<bool> deleteNotes() async {
    var getData = {};
    getData = await _getData();
    await appDatabase.tagDao.deleteTags(getData['getTags']);
    await appDatabase.noteDao.deleteNotes(getData['getNotes']);
    getData = await _getData();

    if (getData['getTags'].isEmpty && getData['getNotes'].isEmpty) {}

    return true;
  }

  @override
  Future<bool> updateNote(NoteAndTagsModel note) async {
    await appDatabase.tagDao.updateTags(note.tags);
    await appDatabase.noteDao.updateNote(note.note);
    return true;
  }

  Future<Map<String, dynamic>> _getData() async {
    final getTags = await appDatabase.tagDao.getTags();
    final getNotes = await appDatabase.noteDao.getNotes();
    Map<String, dynamic> response = {'getTags': getTags, 'getNotes': getNotes};
    return response;
  }
}

// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'app_database.dart';

// **************************************************************************
// FloorGenerator
// **************************************************************************

// ignore: avoid_classes_with_only_static_members
class $FloorAppDatabase {
  /// Creates a database builder for a persistent database.
  /// Once a database is built, you should keep a reference to it and re-use it.
  static _$AppDatabaseBuilder databaseBuilder(String name) =>
      _$AppDatabaseBuilder(name);

  /// Creates a database builder for an in memory database.
  /// Information stored in an in memory database disappears when the process is killed.
  /// Once a database is built, you should keep a reference to it and re-use it.
  static _$AppDatabaseBuilder inMemoryDatabaseBuilder() =>
      _$AppDatabaseBuilder(null);
}

class _$AppDatabaseBuilder {
  _$AppDatabaseBuilder(this.name);

  final String? name;

  final List<Migration> _migrations = [];

  Callback? _callback;

  /// Adds migrations to the builder.
  _$AppDatabaseBuilder addMigrations(List<Migration> migrations) {
    _migrations.addAll(migrations);
    return this;
  }

  /// Adds a database [Callback] to the builder.
  _$AppDatabaseBuilder addCallback(Callback callback) {
    _callback = callback;
    return this;
  }

  /// Creates the database and initializes it.
  Future<AppDatabase> build() async {
    final path = name != null
        ? await sqfliteDatabaseFactory.getDatabasePath(name!)
        : ':memory:';
    final database = _$AppDatabase();
    database.database = await database.open(
      path,
      _migrations,
      _callback,
    );
    return database;
  }
}

class _$AppDatabase extends AppDatabase {
  _$AppDatabase([StreamController<String>? listener]) {
    changeListener = listener ?? StreamController<String>.broadcast();
  }

  NoteDao? _noteDaoInstance;

  TagDao? _tagDaoInstance;

  Future<sqflite.Database> open(
    String path,
    List<Migration> migrations, [
    Callback? callback,
  ]) async {
    final databaseOptions = sqflite.OpenDatabaseOptions(
      version: 1,
      onConfigure: (database) async {
        await database.execute('PRAGMA foreign_keys = ON');
        await callback?.onConfigure?.call(database);
      },
      onOpen: (database) async {
        await callback?.onOpen?.call(database);
      },
      onUpgrade: (database, startVersion, endVersion) async {
        await MigrationAdapter.runMigrations(
            database, startVersion, endVersion, migrations);

        await callback?.onUpgrade?.call(database, startVersion, endVersion);
      },
      onCreate: (database, version) async {
        await database.execute(
            'CREATE TABLE IF NOT EXISTS `notes` (`id` TEXT NOT NULL, `body` TEXT NOT NULL, `title` TEXT NOT NULL, `dateCreated` TEXT NOT NULL, PRIMARY KEY (`id`))');
        await database.execute(
            'CREATE TABLE IF NOT EXISTS `tags` (`id` TEXT NOT NULL, `title` TEXT NOT NULL, `noteId` TEXT NOT NULL, PRIMARY KEY (`id`))');

        await callback?.onCreate?.call(database, version);
      },
    );
    return sqfliteDatabaseFactory.openDatabase(path, options: databaseOptions);
  }

  @override
  NoteDao get noteDao {
    return _noteDaoInstance ??= _$NoteDao(database, changeListener);
  }

  @override
  TagDao get tagDao {
    return _tagDaoInstance ??= _$TagDao(database, changeListener);
  }
}

class _$NoteDao extends NoteDao {
  _$NoteDao(
    this.database,
    this.changeListener,
  )   : _queryAdapter = QueryAdapter(database),
        _noteModelInsertionAdapter = InsertionAdapter(
            database,
            'notes',
            (NoteModel item) => <String, Object?>{
                  'id': item.id,
                  'body': item.body,
                  'title': item.title,
                  'dateCreated': item.dateCreated
                }),
        _noteModelUpdateAdapter = UpdateAdapter(
            database,
            'notes',
            ['id'],
            (NoteModel item) => <String, Object?>{
                  'id': item.id,
                  'body': item.body,
                  'title': item.title,
                  'dateCreated': item.dateCreated
                }),
        _noteModelDeletionAdapter = DeletionAdapter(
            database,
            'notes',
            ['id'],
            (NoteModel item) => <String, Object?>{
                  'id': item.id,
                  'body': item.body,
                  'title': item.title,
                  'dateCreated': item.dateCreated
                });

  final sqflite.DatabaseExecutor database;

  final StreamController<String> changeListener;

  final QueryAdapter _queryAdapter;

  final InsertionAdapter<NoteModel> _noteModelInsertionAdapter;

  final UpdateAdapter<NoteModel> _noteModelUpdateAdapter;

  final DeletionAdapter<NoteModel> _noteModelDeletionAdapter;

  @override
  Future<List<NoteModel>> getNotes() async {
    return _queryAdapter.queryList('SELECT * FROM notes',
        mapper: (Map<String, Object?> row) => NoteModel(
            id: row['id'] as String,
            body: row['body'] as String,
            title: row['title'] as String,
            dateCreated: row['dateCreated'] as String));
  }

  @override
  Future<void> insertNote(NoteModel note) async {
    await _noteModelInsertionAdapter.insert(note, OnConflictStrategy.abort);
  }

  @override
  Future<void> updateNote(NoteModel note) async {
    await _noteModelUpdateAdapter.update(note, OnConflictStrategy.abort);
  }

  @override
  Future<void> deleteNote(NoteModel note) async {
    await _noteModelDeletionAdapter.delete(note);
  }

  @override
  Future<void> deleteNotes(List<NoteModel> notes) async {
    await _noteModelDeletionAdapter.deleteList(notes);
  }
}

class _$TagDao extends TagDao {
  _$TagDao(
    this.database,
    this.changeListener,
  )   : _queryAdapter = QueryAdapter(database),
        _tagModelInsertionAdapter = InsertionAdapter(
            database,
            'tags',
            (TagModel item) => <String, Object?>{
                  'id': item.id,
                  'title': item.title,
                  'noteId': item.noteId
                }),
        _tagModelUpdateAdapter = UpdateAdapter(
            database,
            'tags',
            ['id'],
            (TagModel item) => <String, Object?>{
                  'id': item.id,
                  'title': item.title,
                  'noteId': item.noteId
                }),
        _tagModelDeletionAdapter = DeletionAdapter(
            database,
            'tags',
            ['id'],
            (TagModel item) => <String, Object?>{
                  'id': item.id,
                  'title': item.title,
                  'noteId': item.noteId
                });

  final sqflite.DatabaseExecutor database;

  final StreamController<String> changeListener;

  final QueryAdapter _queryAdapter;

  final InsertionAdapter<TagModel> _tagModelInsertionAdapter;

  final UpdateAdapter<TagModel> _tagModelUpdateAdapter;

  final DeletionAdapter<TagModel> _tagModelDeletionAdapter;

  @override
  Future<List<TagModel>> getTags() async {
    return _queryAdapter.queryList('SELECT * FROM tags',
        mapper: (Map<String, Object?> row) => TagModel(
            id: row['id'] as String,
            title: row['title'] as String,
            noteId: row['noteId'] as String));
  }

  @override
  Future<void> insertTag(TagModel newTag) async {
    await _tagModelInsertionAdapter.insert(newTag, OnConflictStrategy.abort);
  }

  @override
  Future<void> insertTags(List<TagModel> tagList) async {
    await _tagModelInsertionAdapter.insertList(
        tagList, OnConflictStrategy.abort);
  }

  @override
  Future<void> updateTag(TagModel tag) async {
    await _tagModelUpdateAdapter.update(tag, OnConflictStrategy.abort);
  }

  @override
  Future<void> updateTags(List<TagModel> tags) async {
    await _tagModelUpdateAdapter.updateList(tags, OnConflictStrategy.abort);
  }

  @override
  Future<void> deleteTag(TagModel tag) async {
    await _tagModelDeletionAdapter.delete(tag);
  }

  @override
  Future<void> deleteTags(List<TagModel> tagList) async {
    await _tagModelDeletionAdapter.deleteList(tagList);
  }
}

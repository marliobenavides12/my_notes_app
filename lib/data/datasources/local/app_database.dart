import 'dart:async';

import 'package:floor/floor.dart';
import 'package:sqflite/sqflite.dart' as sqflite;

// Model
import 'package:my_notes/data/models/tag_model.dart';
import 'package:my_notes/data/models/note_model.dart';

// Dao
import 'package:my_notes/data/datasources/local/dao/tag_dao.dart';
import 'package:my_notes/data/datasources/local/dao/note_dao.dart';

part 'app_database.g.dart';

@Database(version: 1, entities: [NoteModel, TagModel])
abstract class AppDatabase extends FloorDatabase {
  NoteDao get noteDao;
  TagDao get tagDao;
}

import 'package:floor/floor.dart';

import 'package:my_notes/data/models/tag_model.dart';

@dao
abstract class TagDao {
  @Query('SELECT * FROM tags')
  Future<List<TagModel>> getTags();

  // Insert data
  @insert
  Future<void> insertTag(TagModel newTag);
  @insert
  Future<void> insertTags(List<TagModel> tagList);

  // Update Data
  @update
  Future<void> updateTag(TagModel tag);
  @update
  Future<void> updateTags(List<TagModel> tags);

  // Delet Data
  @delete
  Future<void> deleteTag(TagModel tag);
  @delete
  Future<void> deleteTags(List<TagModel> tagList);
}

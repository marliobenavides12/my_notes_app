import 'package:floor/floor.dart';

import 'package:my_notes/data/models/note_model.dart';

@dao
abstract class NoteDao {
  @Query('SELECT * FROM notes')
  Future<List<NoteModel>> getNotes();

  // Insert data
  @insert
  Future<void> insertNote(NoteModel note);

  // Update Data
  @update
  Future<void> updateNote(NoteModel note);

  // Delet Data
  @delete
  Future<void> deleteNote(NoteModel note);
  @delete
  Future<void> deleteNotes(List<NoteModel> notes);
}

import 'package:flutter/material.dart';

import 'package:uuid/uuid.dart';

import 'package:my_notes/presentation/widgets/button.dart';
import 'package:my_notes/presentation/bloc/bloc_exports.dart';
import 'package:my_notes/core/configs/design/app_colors.dart';
import 'package:my_notes/presentation/widgets/form/form_note.dart';

class NewNotePage extends StatefulWidget {
  const NewNotePage({super.key});

  @override
  State<NewNotePage> createState() => _NewNotePageState();
}

class _NewNotePageState extends State<NewNotePage> {
  @override
  void initState() {
    _dispatchEvent(context);
    super.initState();
  }

  void _dispatchEvent(BuildContext context) {
    const uuid = Uuid();
    final id = uuid.v4();
    BlocProvider.of<NewNoteBloc>(context).add(LoadingEvent(id: id));
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<NewNoteBloc, NewNoteState>(
      builder: (context, state) {
        return WillPopScope(
          onWillPop: () async {
            BlocProvider.of<HomeBloc>(context)
                .add(const GetNotesEvent(note: []));
            Navigator.of(context).pop();
            return true;
          },
          child: Scaffold(
            appBar: AppBar(
              leading: InkWell(
                onTap: () {
                  BlocProvider.of<HomeBloc>(context)
                      .add(const GetNotesEvent(note: []));
                  Navigator.of(context).pop();
                },
                child: const Icon(Icons.arrow_back),
              ),
            ),
            backgroundColor: AppColors.backgroundColor,
            body: Padding(
              padding: const EdgeInsets.fromLTRB(20.0, 0.0, 20.0, 0.0),
              child: SingleChildScrollView(
                child: Column(
                  children: [
                    const FormNote(),
                    Container(
                      margin: const EdgeInsets.symmetric(vertical: 20.0),
                      child: Button(
                        title: 'Guardar',
                        onPressed: () {
                          BlocProvider.of<NewNoteBloc>(context)
                              .add(AddNewNoteEvent());
                        },
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        );
      },
    );
  }
}

import 'package:flutter/material.dart';

import 'package:material_symbols_icons/symbols.dart';

import 'package:my_notes/presentation/widgets/search.dart';
import 'package:my_notes/presentation/widgets/tob_bar.dart';
import 'package:my_notes/core/configs/design/app_colors.dart';
import 'package:my_notes/presentation/bloc/bloc_exports.dart';
import 'package:my_notes/presentation/widgets/note_card.dart';
import 'package:my_notes/presentation/widgets/on_hold/no_data.dart';
import 'package:my_notes/presentation/pages/new_note/new_note_page.dart';

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  void initState() {
    _getNotes(context);
    super.initState();
  }

  void _getNotes(BuildContext context) {
    BlocProvider.of<HomeBloc>(context).add(const GetNotesEvent(note: []));
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<HomeBloc, HomeState>(
      builder: (_, state) {
        return Scaffold(
          appBar: tobBar(context),
          backgroundColor: AppColors.backgroundColor,
          body: state.isLoading
              ? const Center(
                  child: CircularProgressIndicator(color: AppColors.darkBlue),
                )
              : Center(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Expanded(
                        child: CustomScrollView(
                          slivers: [
                            SliverPadding(
                              padding:
                                  const EdgeInsets.symmetric(horizontal: 10),
                              sliver: SliverAppBar(
                                floating: true,
                                elevation: 0.0,
                                titleSpacing: 0.0,
                                title: const Search(),
                                clipBehavior: Clip.none,
                                automaticallyImplyLeading: false,
                                foregroundColor: AppColors.backgroundColor,
                                flexibleSpace: FlexibleSpaceBar(
                                  background: Container(
                                    color: AppColors.backgroundColor,
                                  ),
                                ),
                              ),
                            ),
                            state.notes.isEmpty
                                ? const SliverToBoxAdapter()
                                : SliverPadding(
                                    padding: const EdgeInsets.fromLTRB(
                                        20.0, 10.0, 20.0, 100.0),
                                    sliver: SliverList(
                                      delegate: SliverChildBuilderDelegate(
                                        childCount: state.notes.length,
                                        (_, int index) => NoteCard(
                                          index: index,
                                          showTags: false,
                                          seeMoreText: false,
                                          note: state.notes[index],
                                          withSpaceDown:
                                              state.notes.length == index + 1,
                                        ),
                                      ),
                                    ),
                                  ),
                          ],
                        ),
                      ),
                      state.notes.isEmpty
                          ? const Expanded(child: NoData())
                          : const SizedBox(),
                    ],
                  ),
                ),
          floatingActionButton: FloatingActionButton(
            shape: const CircleBorder(),
            backgroundColor: AppColors.darkBlue,
            onPressed: () {
              Navigator.of(context).push(
                MaterialPageRoute(builder: (context) => const NewNotePage()),
              );
            },
            child: const Icon(
              Symbols.add_rounded,
              size: 40,
              grade: 200,
              weight: 700,
              opticalSize: 20,
              color: Colors.white,
            ),
          ),
        );
      },
    );
  }
}

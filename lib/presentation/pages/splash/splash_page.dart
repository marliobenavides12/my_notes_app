import 'package:flutter/material.dart';

import 'package:my_notes/presentation/bloc/bloc_exports.dart';
import 'package:my_notes/core/configs/design/app_colors.dart';
import 'package:my_notes/presentation/pages/home/home_page.dart';

class SplashPage extends StatefulWidget {
  const SplashPage({super.key});

  @override
  State<SplashPage> createState() => _SplashPageState();
}

class _SplashPageState extends State<SplashPage> {
  @override
  void initState() {
    BlocProvider.of<SplashBloc>(context).add(
      const GoHomeEvent(isLoading: false),
    );
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<SplashBloc, SplashState>(
      listener: (context, state) {
        if (state.isLoading) {
          BlocProvider.of<SplashBloc>(context).add(
            const GoHomeEvent(isLoading: false),
          );
        } else {
          Navigator.of(context).pushReplacement(
            MaterialPageRoute(builder: (context) => const HomePage()),
          );
        }
      },
      child: const Scaffold(
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Spacer(),
              Text(
                'Mis notas',
                style: TextStyle(
                  fontSize: 30,
                  fontWeight: FontWeight.bold,
                  color: AppColors.primaryBlue,
                ),
              ),
              Spacer(),
              Padding(
                padding: EdgeInsets.only(bottom: 20.0),
                child: CircularProgressIndicator(color: AppColors.primaryBlue),
              )
            ],
          ),
        ),
      ),
    );
  }
}

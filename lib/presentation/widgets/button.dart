import 'package:flutter/material.dart';

import 'package:my_notes/core/configs/design/app_colors.dart';

class Button extends StatelessWidget {
  final String title;
  final bool enableButton;
  final void Function()? onPressed;

  const Button({
    super.key,
    this.onPressed,
    required this.title,
    this.enableButton = true,
  });

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Expanded(
          child: ElevatedButton(
            onPressed: enableButton ? onPressed : null,
            style: ElevatedButton.styleFrom(
              backgroundColor: AppColors.primaryBlue,
              foregroundColor: Colors.white,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10),
              ),
            ),
            child: Text(
              title,
              style: const TextStyle(fontSize: 15, fontWeight: FontWeight.w500),
            ),
          ),
        ),
      ],
    );
  }
}

import 'package:flutter/material.dart';

import 'package:material_symbols_icons/symbols.dart';
import 'package:my_notes/presentation/widgets/pop_up.dart';
import 'package:my_notes/core/configs/design/app_colors.dart';
import 'package:my_notes/presentation/bloc/bloc_exports.dart';
import 'package:my_notes/presentation/widgets/on_hold/an_error_occurred.dart';

PreferredSizeWidget? tobBar(BuildContext context) {
  return PreferredSize(
    preferredSize: const Size(double.infinity, 56.0),
    child: AppBar(
      elevation: 0.0,
      forceMaterialTransparency: true,
      title: Row(
        children: [
          const Text(
            'Mis notas',
            style: TextStyle(
              color: AppColors.title,
              fontWeight: FontWeight.w600,
            ),
          ),
          const Spacer(),
          InkWell(
            onTap: () {
              popUp(
                title: 'Advertencia',
                content: const AnErrorOccurred(
                  message: '¿Estás seguro de eliminar todos los datos?',
                ),
                okButton: OkButton(
                  okButton: () {
                    BlocProvider.of<HomeBloc>(context).add(DeleteNotesEvent());
                    Navigator.of(context).pop();
                  },
                ),
                context: context,
              );
            },
            child: const Icon(
              Symbols.delete_sweep,
              size: 36,
              grade: 200,
              weight: 300,
              opticalSize: 40,
              color: AppColors.error,
            ),
          ),
        ],
      ),
      backgroundColor: AppColors.backgroundColor,
    ),
  );
}

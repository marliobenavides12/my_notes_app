import 'package:flutter/material.dart';

import 'package:material_symbols_icons/symbols.dart';

import 'package:my_notes/core/configs/design/app_colors.dart';

class AnErrorOccurred extends StatelessWidget {
  final String message;

  const AnErrorOccurred({super.key, this.message = 'Ups ocurrio un error...'});

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        const Icon(
          Symbols.error_circle_rounded_error,
          size: 40,
          grade: 200,
          weight: 700,
          opticalSize: 20,
          color: AppColors.error,
        ),
        const SizedBox(height: 10.0),
        Text(
          message,
          textAlign: TextAlign.center,
          style: const TextStyle(
            fontSize: 16,
            color: Colors.black45,
            fontWeight: FontWeight.w500,
          ),
        ),
      ],
    );
  }
}

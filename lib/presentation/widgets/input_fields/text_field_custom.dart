import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'package:my_notes/core/configs/design/app_colors.dart';

enum TextFieldType {
  id,
  pin,
  text,
  name,
  email,
  phone,
  number,
  lastName,
  password,
}

class TextFieldCustom extends StatelessWidget {
  final bool enabled;
  final int? minLines;
  final int? maxLines;
  final bool textInside;
  final Color fillColor;
  final bool textOutside;
  final String fieldName;
  final bool obscureText;
  final String? errorText;
  final TextFieldType type;
  final Widget? suffixIcon;
  final FocusNode? focusNode;
  final void Function()? onTap;
  final TextInputType keyboardType;
  final TextEditingController controller;
  final void Function(String)? onChanged;
  final Function(String? value, TextFieldType type)? validator;

  const TextFieldCustom({
    Key? key,
    this.onTap,
    this.minLines,
    this.onChanged,
    this.validator,
    this.errorText,
    this.focusNode,
    this.suffixIcon,
    this.maxLines = 1,
    this.enabled = true,
    this.fieldName = '',
    this.textInside = false,
    this.textOutside = true,
    this.obscureText = false,
    required this.controller,
    this.type = TextFieldType.text,
    this.fillColor = Colors.white,
    this.keyboardType = TextInputType.text,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        validateTextOutside(),
        TextFormField(
          onTap: onTap,
          minLines: minLines,
          maxLines: maxLines,
          onChanged: onChanged,
          focusNode: focusNode,
          controller: controller,
          obscureText: obscureText,
          keyboardType: keyboardType,
          inputFormatters: inputFormatters(type),
          validator: (value) => validator!(value, type),
          decoration: InputDecoration(
            filled: true,
            enabled: enabled,
            errorText: errorText,
            fillColor: fillColor,
            suffixIcon: suffixIcon,
            hoverColor: Colors.white,
            focusColor: Colors.white,
            errorBorder: errorBorder(),
            focusedErrorBorder: errorBorder(),
            hintText: textInside ? fieldName : '',
            contentPadding: const EdgeInsets.all(10),
            border: OutlineInputBorder(
              gapPadding: 10,
              borderSide: BorderSide.none,
              borderRadius: BorderRadius.circular(10.0),
            ),
          ),
        ),
      ],
    );
  }

  Widget validateTextOutside() => textOutside
      ? Column(children: [
          Text(
            fieldName,
            style: const TextStyle(
              color: AppColors.title,
              fontWeight: FontWeight.w500,
            ),
          ),
          const SizedBox(height: 10.0)
        ])
      : const SizedBox();

  InputBorder errorBorder() => OutlineInputBorder(
        gapPadding: 10,
        borderRadius: BorderRadius.circular(10.0),
        borderSide: const BorderSide(width: 1, color: Colors.red),
      );

  List<TextInputFormatter>? inputFormatters(TextFieldType type) {
    return <TextInputFormatter>[] +
        ((type == TextFieldType.id ||
                type == TextFieldType.pin ||
                type == TextFieldType.phone ||
                type == TextFieldType.number)
            ? [
                FilteringTextInputFormatter.deny(RegExp(r"\s")),
                FilteringTextInputFormatter.allow(RegExp(r'[0-9]')),
              ]
            : []) +
        ((type == TextFieldType.email || type == TextFieldType.password)
            ? [FilteringTextInputFormatter.deny(RegExp(r"\s"))]
            : []);
  }
}

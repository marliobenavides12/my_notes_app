import 'package:flutter/material.dart';
import 'package:flutter/gestures.dart';

import 'package:material_symbols_icons/symbols.dart';

import 'package:my_notes/presentation/widgets/tag.dart';
import 'package:my_notes/presentation/bloc/bloc_exports.dart';
import 'package:my_notes/core/configs/design/app_colors.dart';
import 'package:my_notes/data/models/note_ans_tags_model.dart';

class NoteCard extends StatelessWidget {
  final int index;
  final bool showTags;
  final bool seeMoreText;
  final bool withSpaceDown;
  final NoteAndTagsModel note;

  const NoteCard({
    super.key,
    required this.note,
    required this.index,
    required this.showTags,
    required this.seeMoreText,
    required this.withSpaceDown,
  });

  @override
  Widget build(BuildContext context) {
    return Dismissible(
      key: ValueKey(index),
      confirmDismiss: (direction) async {
        return true;
      },
      onDismissed: (direction) {
        if (direction == DismissDirection.startToEnd ||
            direction == DismissDirection.endToStart) {
          BlocProvider.of<HomeBloc>(context)
              .add(DeleteNoteEvent(note: note, index: index));
        }
      },
      background: configBackground(Alignment.centerLeft),
      secondaryBackground: configBackground(Alignment.centerRight),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.end,
        children: [
          InkWell(
            onTap: () {
              BlocProvider.of<HomeBloc>(context).add(
                DeleteNoteEvent(note: note, index: index),
              );
            },
            child: const Material(
              color: Colors.white,
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(20),
                topRight: Radius.circular(20),
              ),
              child: Padding(
                padding: EdgeInsets.fromLTRB(8, 4, 8, 0),
                child: Icon(Icons.delete_forever,
                    size: 18, color: AppColors.error),
              ),
              // chil
            ),
          ),
          SizedBox(
            child: Column(
              children: [
                Material(
                  color: Colors.white,
                  borderRadius: const BorderRadius.only(
                    topLeft: Radius.circular(10.0),
                    bottomLeft: Radius.circular(10.0),
                    bottomRight: Radius.circular(10.0),
                  ),
                  child: Column(
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(
                          top: 10,
                          left: 10,
                          right: 10,
                          bottom: 10,
                        ),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.end,
                          children: [
                            Align(
                              alignment: Alignment.centerLeft,
                              child: Text(
                                note.note.title,
                                style: const TextStyle(
                                  fontSize: 16,
                                  color: AppColors.title,
                                  fontWeight: FontWeight.w600,
                                ),
                              ),
                            ),
                            const SizedBox(height: 10),
                            Align(
                              alignment: Alignment.centerLeft,
                              child: RichText(
                                text: TextSpan(
                                  children: [
                                    TextSpan(
                                      text: note.note.body,
                                      style: const TextStyle(
                                        color: AppColors.subTitle,
                                        fontWeight: FontWeight.w400,
                                      ),
                                    ),
                                    TextSpan(
                                      text: ' see more',
                                      style: const TextStyle(
                                        fontWeight: FontWeight.w400,
                                        color: AppColors.primaryBlue,
                                      ),
                                      recognizer: TapGestureRecognizer()
                                        ..onTap = () {},
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            const SizedBox(height: 10),
                            SizedBox(
                              height: showTags ? null : 32,
                              child: Wrap(
                                clipBehavior:
                                    showTags ? Clip.none : Clip.hardEdge,
                                alignment: WrapAlignment.end,
                                children: [
                                  ...List.generate(
                                    note.tags.length,
                                    (index) =>
                                        Tag(title: note.tags[index].title),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                      InkWell(
                        // onTap: ,
                        child: SizedBox(
                          width: double.infinity,
                          child: Material(
                            color: Colors.white,
                            borderRadius: const BorderRadius.only(
                              bottomLeft: Radius.circular(10.0),
                              bottomRight: Radius.circular(10.0),
                            ),
                            child: Icon(
                              showTags
                                  ? Symbols.expand_less
                                  : Symbols.expand_more,
                              size: 18,
                              grade: 200,
                              weight: 700,
                              opticalSize: 20,
                              color: AppColors.darkGrey,
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
          withSpaceDown ? const SizedBox() : const SizedBox(height: 10),
        ],
      ),
    );
  }

  Widget configBackground(AlignmentGeometry alignment) {
    return Container(
      alignment: alignment,
      color: AppColors.error,
      padding: const EdgeInsets.only(left: 10, right: 10),
      child: const Icon(Icons.delete_forever, size: 28, color: Colors.white),
    );
  }
}

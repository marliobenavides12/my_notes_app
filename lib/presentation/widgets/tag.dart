import 'package:flutter/material.dart';

import 'package:material_symbols_icons/symbols.dart';

import 'package:my_notes/core/configs/design/app_colors.dart';

class Tag extends StatelessWidget {
  final String title;
  final bool shoeDeleteTag;
  final void Function()? onTap;

  const Tag({
    super.key,
    this.onTap,
    required this.title,
    this.shoeDeleteTag = false,
  });

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 3.0, horizontal: 3.0),
      child: Material(
        color: AppColors.lightGrey,
        borderRadius: BorderRadius.circular(6.0),
        child: Padding(
          padding: const EdgeInsets.symmetric(vertical: 3.0, horizontal: 8.0),
          child: Row(
            mainAxisSize: MainAxisSize.min,
            children: [
              Text(
                title,
                style: const TextStyle(
                  fontSize: 13.0,
                  color: AppColors.subTitle,
                ),
              ),
              const SizedBox(width: 6.0),
              shoeDeleteTag
                  ? InkWell(
                      onTap: onTap,
                      child: const Icon(
                        Symbols.close_rounded,
                        size: 16,
                        grade: 200,
                        weight: 700,
                        opticalSize: 20,
                        color: AppColors.subTitle,
                      ),
                    )
                  : const SizedBox(),
            ],
          ),
        ),
      ),
    );
  }
}

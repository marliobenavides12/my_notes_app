import 'package:flutter/material.dart';

import 'package:expandable/expandable.dart';

import 'package:my_notes/presentation/widgets/tag.dart';
import 'package:my_notes/presentation/widgets/pop_up.dart';
import 'package:my_notes/presentation/bloc/bloc_exports.dart';
import 'package:my_notes/core/configs/design/app_colors.dart';
import 'package:my_notes/presentation/widgets/input_fields/text_field_custom.dart';

class FormNote extends StatelessWidget {
  const FormNote({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<NewNoteBloc, NewNoteState>(
      builder: (context, state) {
        return Form(
          key: state.formKey,
          child: Column(
            children: [
              TextFieldCustom(
                fieldName: 'Título',
                controller: state.titleController,
              ),
              const SizedBox(height: 10.0),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const Text(
                    'Tags',
                    style: TextStyle(
                      color: AppColors.title,
                      fontWeight: FontWeight.w500,
                    ),
                  ),
                  const SizedBox(height: 10.0),
                  Material(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(10.0),
                    child: Padding(
                      padding: const EdgeInsets.fromLTRB(8.0, 0.0, 8.0, 8.0),
                      child: ExpandablePanel(
                        theme: const ExpandableThemeData(
                          iconSize: 28,
                          headerAlignment:
                              ExpandablePanelHeaderAlignment.center,
                        ),
                        header: Row(
                          children: [
                            InkWell(
                              onTap: () {
                                popUp(
                                  title: 'Nombre del tag',
                                  content: TextFieldCustom(
                                    fieldName: 'Título',
                                    controller: state.tagTitleController,
                                    fillColor: AppColors.backgroundColor,
                                  ),
                                  okButton: OkButton(
                                    okButton: () {
                                      BlocProvider.of<NewNoteBloc>(context)
                                          .add(AddTagEvent());
                                      Navigator.of(context).pop();
                                    },
                                  ),
                                  context: context,
                                );
                              },
                              child: const Text(
                                'Añadir +',
                                style: TextStyle(color: AppColors.primaryBlue),
                              ),
                            ),
                          ],
                        ),
                        collapsed: SizedBox(
                          height: 30,
                          child: Wrap(
                            clipBehavior: Clip.hardEdge,
                            alignment: WrapAlignment.end,
                            children: [
                              ...List.generate(
                                state.tags.length,
                                (index) => Tag(
                                  shoeDeleteTag: true,
                                  title: state.tags[index].title,
                                ),
                              ),
                            ],
                          ),
                        ),
                        expanded: Column(
                          children: [
                            Wrap(
                              alignment: WrapAlignment.end,
                              children: [
                                ...List.generate(
                                  state.tags.length,
                                  (index) => Tag(
                                    shoeDeleteTag: true,
                                    title: state.tags[index].title,
                                  ),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ],
              ),
              const SizedBox(height: 10.0),
              TextFieldCustom(
                minLines: 6,
                maxLines: null,
                fieldName: 'Descripción',
                keyboardType: TextInputType.multiline,
                controller: state.descriptionController,
              ),
            ],
          ),
        );
      },
    );
  }
}

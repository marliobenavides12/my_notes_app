import 'package:flutter/material.dart';

import 'package:my_notes/core/configs/design/app_colors.dart';

class Search extends StatelessWidget {
  const Search({super.key});

  @override
  Widget build(BuildContext context) {
    return Material(
      elevation: 4,
      borderRadius: BorderRadius.circular(20),
      child: SizedBox(
        height: 50,
        child: TextFormField(
          style: const TextStyle(
            color: Color(0xFF676A6C),
            fontWeight: FontWeight.w500,
          ),
          cursorColor: const Color(0xFF333333),
          decoration: InputDecoration(
            filled: true,
            hintText: 'Search',
            hintStyle: const TextStyle(color: AppColors.mediumGrey),
            fillColor: Colors.white,
            focusColor: Colors.white,
            hoverColor: Colors.white,
            prefixIcon: const Icon(Icons.search),
            contentPadding: const EdgeInsets.symmetric(
              vertical: 0.0,
              horizontal: 16.0,
            ),
            enabledBorder: OutlineInputBorder(
              gapPadding: 10.0,
              borderRadius: BorderRadius.circular(20),
              borderSide: const BorderSide(
                color: Colors.white,
              ),
            ),
            focusedBorder: OutlineInputBorder(
              gapPadding: 10.0,
              borderRadius: BorderRadius.circular(20),
              borderSide: const BorderSide(
                color: Colors.white,
                // color: Color(value)
                // color: Color(0xFF333333),
              ),
            ),
            border: OutlineInputBorder(
              gapPadding: 10.0,
              borderRadius: BorderRadius.circular(20),
              borderSide: const BorderSide(
                color: Colors.white,
              ),
            ),
          ),
        ),
      ),
    );
  }
}

part of 'new_note_bloc.dart';

class NewNoteState extends Equatable {
  final String noteId;
  final List<TagModel> tags;
  final NoteAndTagsModel note;

  final GlobalKey<FormState> formKey;
  final TextEditingController titleController;
  final TextEditingController tagTitleController;
  final TextEditingController descriptionController;

  const NewNoteState({
    required this.tags,
    required this.note,
    required this.noteId,
    required this.formKey,
    required this.titleController,
    required this.tagTitleController,
    required this.descriptionController,
  });

  factory NewNoteState.initial() => NewNoteState(
        noteId: '',
        tags: const [],
        formKey: GlobalKey<FormState>(),
        note: NoteAndTagsModel.formNoData(),
        titleController: TextEditingController(),
        tagTitleController: TextEditingController(),
        descriptionController: TextEditingController(),
      );

  NewNoteState copyWiht({
    String? noteId,
    List<TagModel>? tags,
    NoteAndTagsModel? note,
    GlobalKey<FormState>? formKey,
    TextEditingController? titleController,
    TextEditingController? tagTitleController,
    TextEditingController? descriptionController,
  }) =>
      NewNoteState(
        note: note ?? this.note,
        tags: tags ?? this.tags,
        noteId: noteId ?? this.noteId,
        formKey: formKey ?? this.formKey,
        titleController: titleController ?? this.titleController,
        descriptionController:
            descriptionController ?? this.descriptionController,
        tagTitleController: tagTitleController ?? this.tagTitleController,
      );

  @override
  List<Object> get props => [
        tags,
        note,
        noteId,
        formKey,
        titleController,
        tagTitleController,
        descriptionController,
      ];
}

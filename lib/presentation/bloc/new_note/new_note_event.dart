part of 'new_note_bloc.dart';

abstract class NewNoteEvent extends Equatable {
  const NewNoteEvent();

  @override
  List<Object> get props => [];
}

class LoadingEvent extends NewNoteEvent {
  final String id;

  const LoadingEvent({required this.id});

  @override
  List<Object> get props => [id];
}

class AddTagEvent extends NewNoteEvent {}

class AddNewNoteEvent extends NewNoteEvent {}

class UpdateEvent extends NewNoteEvent {}

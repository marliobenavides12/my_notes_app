import 'package:flutter/material.dart';

import 'package:uuid/uuid.dart';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';

import 'package:my_notes/data/models/tag_model.dart';
import 'package:my_notes/data/models/note_model.dart';
import 'package:my_notes/data/models/note_ans_tags_model.dart';
import 'package:my_notes/domain/use_cases/add_note_use_case.dart';
import 'package:my_notes/domain/use_cases/update_note_use_case.dart';

part 'new_note_event.dart';
part 'new_note_state.dart';

class NewNoteBloc extends Bloc<NewNoteEvent, NewNoteState> {
  final Uuid uuid;
  final AddNoteUseCase addNoteUseCase;
  final UpdateNoteUseCase updateNoteUseCase;

  NewNoteBloc({
    required this.uuid,
    required this.addNoteUseCase,
    required this.updateNoteUseCase,
  }) : super(NewNoteState.initial()) {
    on<NewNoteEvent>((event, emit) {});
    on<AddTagEvent>(_onAddTag);
    on<LoadingEvent>(_onLoading);
    on<AddNewNoteEvent>(_onAddNewNote);
  }
  _onLoading(LoadingEvent event, Emitter<NewNoteState> emit) async {
    final id = uuid.v4();
    emit(state.copyWiht(noteId: id));
  }

  _onAddTag(AddTagEvent event, Emitter<NewNoteState> emit) async {
    final id = uuid.v4();

    TagModel newTag = TagModel(
      id: id,
      noteId: state.noteId,
      title: state.tagTitleController.text,
    );

    List<TagModel> tags = [...state.tags, newTag];

    emit(state.copyWiht(tags: tags));
  }

  _onAddNewNote(AddNewNoteEvent event, Emitter<NewNoteState> emit) async {
    final newNote = NoteAndTagsModel(
      tags: state.tags,
      note: NoteModel(
        id: state.noteId,
        title: state.titleController.text,
        body: state.descriptionController.text,
        dateCreated: DateTime.now().toString(),
      ),
    );

    final addNewNote = await addNoteUseCase.addNote(newNote);

    if (addNewNote) {
      emit(NewNoteState.initial());
    }
  }
}

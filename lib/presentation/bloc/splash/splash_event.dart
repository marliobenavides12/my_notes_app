part of 'splash_bloc.dart';

abstract class SplashEvent extends Equatable {
  const SplashEvent();

  @override
  List<Object> get props => [];
}

class GoHomeEvent extends SplashEvent {
  final bool isLoading;

  const GoHomeEvent({required this.isLoading});

  @override
  List<Object> get props => [isLoading];
}

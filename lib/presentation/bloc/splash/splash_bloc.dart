import 'package:bloc/bloc.dart';

import 'package:equatable/equatable.dart';

part 'splash_event.dart';
part 'splash_state.dart';

class SplashBloc extends Bloc<SplashEvent, SplashState> {
  SplashBloc() : super(const SplashState()) {
    on<GoHomeEvent>(_goHome);
  }

  _goHome(GoHomeEvent event, Emitter<SplashState> emit) {
    Future.delayed(const Duration(seconds: 4));
    emit(state.copyWiht(isLoading: event.isLoading));
  }
}

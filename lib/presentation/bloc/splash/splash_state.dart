part of 'splash_bloc.dart';

class SplashState extends Equatable {
  final bool isLoading;

  const SplashState({this.isLoading = true});

  factory SplashState.initial() => const SplashState(isLoading: true);

  SplashState copyWiht({bool? isLoading}) => SplashState(
        isLoading: isLoading ?? this.isLoading,
      );

  @override
  List<Object> get props => [isLoading];
}

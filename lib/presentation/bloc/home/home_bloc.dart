import 'package:bloc/bloc.dart';

import 'package:equatable/equatable.dart';

import 'package:my_notes/data/models/note_ans_tags_model.dart';
import 'package:my_notes/domain/use_cases/get_notes_use_case.dart';
import 'package:my_notes/domain/use_cases/delete_note_use_case.dart';
import 'package:my_notes/domain/use_cases/delete_notes_use_case.dart';

part 'home_event.dart';
part 'home_state.dart';

class HomeBloc extends Bloc<HomeEvent, HomeState> {
  final GetNotesUseCase getNoteUseCase;
  final DeleteNoteUseCase deleteNoteUseCase;
  final DeleteNotesUseCase deleteNotesUseCase;

  HomeBloc({
    required this.getNoteUseCase,
    required this.deleteNoteUseCase,
    required this.deleteNotesUseCase,
  }) : super(HomeState.initial()) {
    on<GetNotesEvent>(_onGetNotes);
    on<DeleteNoteEvent>(_onDeleteNote);
    on<DeleteNotesEvent>(_onDeleteNotes);
  }

  void _onGetNotes(GetNotesEvent event, Emitter<HomeState> emit) async {
    final List<NoteAndTagsModel> getNotes = await getNoteUseCase.getNotes();
    Future.delayed(const Duration(seconds: 2));
    emit(state.copyWiht(notes: getNotes, isLoading: false));
  }

  void _onDeleteNote(DeleteNoteEvent event, Emitter<HomeState> emit) async {
    await deleteNoteUseCase.deleteNote(event.note);
    List<NoteAndTagsModel> notes = [...state.notes];
    notes.removeAt(event.index);
    emit(state.copyWiht(notes: notes, isLoading: false));
  }

  void _onDeleteNotes(DeleteNotesEvent event, Emitter<HomeState> emit) async {
    emit(state.copyWiht(isLoading: true));
    await deleteNotesUseCase.deleteNotes();
    Future.delayed(const Duration(seconds: 2));
    emit(state.copyWiht(notes: [], isLoading: false));
  }
}

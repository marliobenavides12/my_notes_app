part of 'home_bloc.dart';

class HomeState extends Equatable {
  final bool isLoading;
  final List<NoteAndTagsModel> notes;

  const HomeState({
    required this.notes,
    required this.isLoading,
  });

  factory HomeState.initial() => const HomeState(
        notes: [],
        isLoading: true,
      );

  HomeState copyWiht({
    bool? isLoading,
    List<NoteAndTagsModel>? notes,
  }) =>
      HomeState(
        notes: notes ?? this.notes,
        isLoading: isLoading ?? this.isLoading,
      );

  @override
  List<Object> get props => [notes, isLoading];
}

part of 'home_bloc.dart';

abstract class HomeEvent extends Equatable {
  const HomeEvent();

  @override
  List<Object> get props => [];
}

class GetNotesEvent extends HomeEvent {
  final List<NoteAndTagsModel> note;

  const GetNotesEvent({required this.note});

  @override
  List<Object> get props => [note];
}

class AddNoteEvent extends HomeEvent {
  final NoteAndTagsModel note;

  const AddNoteEvent({required this.note});

  @override
  List<Object> get props => [note];
}

class DeleteNoteEvent extends HomeEvent {
  final int index;
  final NoteAndTagsModel note;

  const DeleteNoteEvent({required this.note, required this.index});

  @override
  List<Object> get props => [note];
}

class DeleteNotesEvent extends HomeEvent {}

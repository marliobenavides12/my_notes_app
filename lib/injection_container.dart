import 'package:uuid/uuid.dart';
import 'package:get_it/get_it.dart';

import 'package:my_notes/data/datasources/datasource.dart';
import 'package:my_notes/presentation/bloc/bloc_exports.dart';
import 'package:my_notes/data/repositories/repository_impl.dart';
import 'package:my_notes/domain/use_cases/add_note_use_case.dart';
import 'package:my_notes/domain/repositories/repository_impl.dart';
import 'package:my_notes/data/datasources/local/app_database.dart';
import 'package:my_notes/domain/use_cases/get_notes_use_case.dart';
import 'package:my_notes/domain/use_cases/update_note_use_case.dart';
import 'package:my_notes/domain/use_cases/delete_note_use_case.dart';
import 'package:my_notes/domain/use_cases/delete_notes_use_case.dart';

final sl = GetIt.instance;

Future<void> initDependencies() async {
  // ---------------------------------------------------------------------
  //                               Data base
  // ---------------------------------------------------------------------
  var db = await $FloorAppDatabase.databaseBuilder('app_database.db').build();
  sl.registerSingleton<AppDatabase>(db);

  // ---------------------------------------------------------------------
  //                                 Blocs
  // ---------------------------------------------------------------------
  sl.registerFactory<SplashBloc>(() => SplashBloc());
  sl.registerFactory<HomeBloc>(() => HomeBloc(
        getNoteUseCase: sl(),
        deleteNoteUseCase: sl(),
        deleteNotesUseCase: sl(),
      ));
  sl.registerFactory<NewNoteBloc>(() => NewNoteBloc(
        uuid: sl(),
        addNoteUseCase: sl(),
        updateNoteUseCase: sl(),
      ));

  // ---------------------------------------------------------------------
  //                               Use cases
  // ---------------------------------------------------------------------
  sl.registerLazySingleton(() => AddNoteUseCase(sl()));
  sl.registerLazySingleton(() => GetNotesUseCase(sl()));
  sl.registerLazySingleton(() => UpdateNoteUseCase(sl()));
  sl.registerLazySingleton(() => DeleteNotesUseCase(sl()));
  sl.registerLazySingleton(() => DeleteNoteUseCase(sl()));

  // ---------------------------------------------------------------------
  //                              Repositories
  // ---------------------------------------------------------------------

  sl.registerLazySingleton<Repository>(() => RepositoryImpl(sl()));

  // ---------------------------------------------------------------------
  //                               DataSource
  // ---------------------------------------------------------------------

  sl.registerLazySingleton<DataSource>(() => DataSourceImpl(sl()));

  // ---------------------------------------------------------------------
  //                           Generate ID random
  // ---------------------------------------------------------------------
  sl.registerFactory<Uuid>(() => const Uuid());
}

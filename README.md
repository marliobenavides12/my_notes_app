# my_notes #

App para toma de notas; es un proyecto creado con flutter .

## Comenzando ##

Aclaro este proyecto aún no está al 100% terminado, pero puede darte una idea o perspectiva de como implementar Clean Architecture en proyectos Flutter.

### Paquetes utilizados ###

  [uuid: ](https://pub.dev/packages/uuid)
  [floor: ](https://pub.dev/packages/floor)
  [get_it: ](https://pub.dev/packages/get_it)
  [sqflite: ](https://pub.dev/packages/sqflite)
  [equatable: ](https://pub.dev/packages/equatable)
  [expandable: ](https://pub.dev/packages/expandable)
  [flutter_bloc: ](https://pub.dev/packages/flutter_bloc)

### Pre-requisitos ###

- Flutter 3.13.4
- Dart 3.1.2

### Instalación ###

Seguir las instrucciones del siguiente link: [Proceso de instalación](https://flutter.dev/docs/get-started/install)

### Para obtener las últimas dependencias ### ###

```shell
flutter pub get
```

## Para ejecutar el generador de código, ejecute el siguiente comando ## ##

```shell
flutter pub run build_runner build --delete-conflicting-outputs
```

## Para proyectos de Flutter, también puedes ejecutar ## ##

```shell
flutter pub run build_runner build
```